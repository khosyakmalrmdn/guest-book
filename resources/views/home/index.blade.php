@extends('layout.app')
@section('content')

<section class="masthead h-100 about-section text-center pt-5" id="form">
    <div class="container h-100">
        <div class="text-brown fw-bolder fs-1 py-4">The Wedding Of Siska & Arda</div>
        <div class="row pb-5">
            <div class="col-md-4">
                <div class="card w-100">
                    <div class="card-body">
                        <form id="add-guest" method="post" enctype="multipart/form-data">
                            <div class="row gx-4 gx-lg-5 justify-content-center">
                                <div class="col-md-12 py-3">
                                    <div class="text-start text-brown fw-bolder fs-5">Nama</div>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Masukan Nama Tamu">
                                    <div class="valid text-start text-danger" id="nameValidator"></div>
                                </div>
                                <div class="col-md-12 py-3">
                                    <div class="text-start text-brown fw-bolder fs-5">Alamat</div>
                                    <textarea class="form-control" placeholder="Masukan Alamat Tamu" name="address" id="address" cols="10" rows="5"></textarea>
                                    <div class="valid text-danger text-start" id="addressValidator"></div>
                                </div>
                                <div class="d-flex justify-content-center pb-4">
                                    <div class="col-md-12">
                                        <button type="submit" id="saveGuest" class="w-100">
                                            <span class="spinner-border spinner-border-sm d-none" id="loadingButtonSave"></span>
                                            <span id="textButtonSave" class="text-dark fw-bolder"> SIMPAN</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-lg-12 pb-3">
                        <div class="card h-100 w-100">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="mytable" id="tableGuest">
                                        <thead>
                                            <th>-</th>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th class="text-start">Alamat</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var datatable;
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(function() {
        datatable = $('#tableGuest').DataTable({
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            processing: true,
            serverSide: true,
            ajax: "{{ url()->current() }}",
            columns: [
                {data: 'created_at'},
                {data: 'DT_RowIndex', width:'10%'},
                {data: 'name', width:'30%'},
                {data: 'address'},
            ],
            order: [[1, 'desc']],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
    });

    setInterval(function() { $('#tableGuest').DataTable().ajax.reload(); }, 5000);


    $('#add-guest').submit(function (e) {
        e.preventDefault();
        $(".valid").text("");
        $("#loadingButtonSave").removeClass('d-none');
        $("#textButtonSave").addClass('d-none');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: '{{ route("guest.post") }}',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            error: function (err) {
                $("#loadingButtonSave").addClass('d-none');
                $("#textButtonSave").removeClass('d-none');
                if(err.responseJSON.error.name){
                    $("#nameValidator").text('Nama harus di isi');
                }
                if(err.responseJSON.error.address){
                    $("#addressValidator").text('Alamat harus di isi');
                }

                iziToast.show({
                    title: 'Warning',
                    message: 'Harap mengisikan data dengan lengkap',
                    position: 'bottomLeft',
                    color:'yellow'
                });
            },
            success: function (response) {
                if (response.status == 200 || response.status == 201) {
                    $("#loadingButtonSave").addClass('d-none');
                    $("#textButtonSave").removeClass('d-none');
                    $("#add-guest")[0].reset();
                    $('#tableGuest').DataTable().ajax.reload();
                    $("#name").focus();
                    iziToast.show({
                        title: 'Success',
                        message: 'Berhasil menambahkan tamu',
                        position: 'bottomLeft',
                        color:'green'
                    });
                } else {
                    $("#loadingButtonSave").addClass('d-none');
                    $("#textButtonSave").removeClass('d-none');
                    Swal.fire(
                        'Error!',
                        'Error Input Tamu!',
                        'error'
                    );
                }
            },
        })
    })
</script>
@endsection
