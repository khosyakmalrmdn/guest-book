@extends('layout.app')
@section('content')

<section class="masthead h-100 about-section text-center pt-2" id="form">
    <div class="container">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-12">
                <h2 class="mb-4 mt-4">Manajemen Tamu</h2>
            </div>
            <div class="col-lg-12 py-3">
                <div class="card h-100 w-100">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="mytable" id="tableGuest">
                                <thead>
                                    <th>-</th>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Nominal</th>
                                    <th>Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            </div>
            <form id="add">
                <div class="modal-body">
                    <div class="row gx-4 gx-lg-5 justify-content-center">
                        <div class="col-md-12 py-3">
                            <div class="text-start text-brown fw-bolder fs-5">Nama</div>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Masukan Nama Tamu">
                            <div class="valid text-start text-danger" id="nameValidator"></div>
                        </div>
                        <div class="col-md-12 py-3">
                            <div class="text-start text-brown fw-bolder fs-5">Alamat</div>
                            <textarea class="form-control" placeholder="Masukan Alamat Tamu" name="address" id="address" cols="10" rows="5"></textarea>
                            <div class="valid text-danger text-start" id="addressValidator"></div>
                        </div>
                        <div class="col-md-12 py-3">
                            <div class="text-start text-brown fw-bolder fs-5">Nominal</div>
                            <input type="text" class="form-control" name="nominal" id="nominal" placeholder="Masukan Nominal">
                            <div class="valid text-start text-danger" id="nominalValidator"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary close-btn" data-dismiss="modal">BATAL</button>
                    <button type="submit" class="btn btn-primary">
                        <span class="spinner-border spinner-border-sm d-none" id="loadingButtonSave"></span>
                        <span id="textButtonSave" class="text-dark fw-bolder"> SIMPAN</span>
                    </button>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            </div>
            <form id="update">
                <div class="modal-body">
                    <div class="row gx-4 gx-lg-5 justify-content-center">
                        <div class="col-md-12 py-3">
                            <div class="text-start text-brown fw-bolder fs-5">Nama</div>
                            <input type="text" class="form-control" name="name" id="nameUpdate" placeholder="Masukan Nama Tamu">
                            <div class="valid text-start text-danger" id="nameValidator"></div>
                        </div>
                        <div class="col-md-12 py-3">
                            <div class="text-start text-brown fw-bolder fs-5">Alamat</div>
                            <textarea class="form-control" placeholder="Masukan Alamat Tamu" name="address" id="addressUpdate" cols="10" rows="5"></textarea>
                            <div class="valid text-danger text-start" id="addressValidator"></div>
                        </div>
                        <div class="col-md-12 py-3">
                            <div class="text-start text-brown fw-bolder fs-5">Nominal</div>
                            <input type="text" class="form-control" name="nominal" id="nominalUpdate" placeholder="Masukan Nominal">
                            <div class="valid text-start text-danger" id="nominalValidator"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary close-btn" data-dismiss="modal">BATAL</button>
                    <button type="submit" class="btn btn-primary">
                        <span class="spinner-border spinner-border-sm d-none" id="loadingButtonUpdate"></span>
                        <span id="textButtonUpdate" class="text-dark fw-bolder"> SIMPAN</span>
                    </button>
                </div>
            </form>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).on('click', '.edit', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var token = $('meta[name="csrf-token"]').attr('content');
        $('#modalUpdate').modal('show');
        $('#update').data('id', id);
        var url = "{{ route('guest.get.data', ":id") }}";
        url = url.replace(':id', id);
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                $("#nameUpdate").val(data.name);
                $("#addressUpdate").val(data.address);
                $("#nominalUpdate").val(convertRupiah(data.nominal));
            },
        });
        return false;
    })

    $(document).on('click', '.add', function(){
        var id = $(this).data('id');
        $('#modalAdd').modal('show');
    })

    $(document).on('click', '.close-btn', function(){
        $('#modalInput').modal('hide');
    })

    $(document).on("input", "#nominal,#nominalUpdate ", function () {
            $(this).val(formatRupiah(this.value));
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? + rupiah : '');
    }

    function convertRupiah(value){
        var price = value;
        var formatPrice = Math.round(price).toString().split('').reverse().join('');
        var convertPrice = formatPrice.match(/\d{1,3}/g);
        var rupiahPrice = convertPrice.join('.').split('').reverse().join('');
        return rupiahPrice;
    }
  </script>

<script>
    var datatable;
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(function() {
        datatable = $('#tableGuest').DataTable({
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            processing: true,
            serverSide: true,
            ajax: "{{ url()->current() }}",
            columns: [
                {data: 'created_at'},
                {data: 'DT_RowIndex', width:'5%'},
                {data: 'name', width:'20%'},
                {data: 'address', width:'50%'},
                {data: 'nominal', width:'15%'},
                {data: 'action', width:'5%'},
            ],
            order: [[1, 'desc']],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
    });

    $('#add').submit(function (e) {
        e.preventDefault();
        $(".valid").text("");
        $("#loadingButtonSave").removeClass('d-none');
        $("#textButtonSave").addClass('d-none');
        var route = '{{ route("guest.full.post") }}';
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: route,
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            error: function (err) {
                $("#loadingButtonSave").addClass('d-none');
                $("#textButtonSave").removeClass('d-none');
                if(err.responseJSON.error.name){
                    $("#nameValidator").text('Nama harus di isi');
                }

                if(err.responseJSON.error.address){
                    $("#addressValidator").text('Alamat harus di isi');
                }

                if(err.responseJSON.error.nominal){
                    $("#nominalValidator").text('Nominal harus di isi');
                }

                iziToast.show({
                    title: 'Warning',
                    message: 'Harap mengisikan data dengan lengkap',
                    position: 'bottomLeft',
                    color:'yellow'
                });
            },
            success: function (response) {
                if (response.status == 200 || response.status == 201) {
                    $("#loadingButtonSave").addClass('d-none');
                    $("#textButtonSave").removeClass('d-none');
                    $("#add")[0].reset();
                    $('#tableGuest').DataTable().ajax.reload();
                    $("#modalAdd").modal("hide");
                    iziToast.show({
                        title: 'Success',
                        message: 'Berhasil menambahkan tamu',
                        position: 'bottomLeft',
                        color:'green'
                    });
                } else {
                    $("#loadingButtonSave").addClass('d-none');
                    $("#textButtonSave").removeClass('d-none');
                    Swal.fire(
                        'Error!',
                        'Error Input Tamu!',
                        'error'
                    );
                }
            },
        })
    })

    $('#update').submit(function (e) {
        e.preventDefault();
        $(".valid").text("");
        $("#loadingButtonUpdate").removeClass('d-none');
        $("#textButtonUpdate").addClass('d-none');

        var id = $(this).data('id');
        var url = "{{ route('guest.update', ":id") }}" ;
            url = url.replace(':id', id);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: url,
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            error: function (err) {
                $("#loadingButtonUpdate").addClass('d-none');
                $("#textButtonUpdate").removeClass('d-none');
                if(err.responseJSON.error.name){
                    $("#nameValidator").text('Nama harus di isi');
                }

                if(err.responseJSON.error.address){
                    $("#addressValidator").text('Alamat harus di isi');
                }

                if(err.responseJSON.error.nominal){
                    $("#nominalValidator").text('Nominal harus di isi');
                }

                iziToast.show({
                    title: 'Warning',
                    message: 'Harap mengisikan data dengan lengkap',
                    position: 'bottomLeft',
                    color:'yellow'
                });
            },
            success: function (response) {
                if (response.status == 200 || response.status == 201) {
                    $("#loadingButtonUpdate").addClass('d-none');
                    $("#textButtonUpdate").removeClass('d-none');
                    $('#tableGuest').DataTable().ajax.reload();
                    $("#modalUpdate").modal("hide");
                    iziToast.show({
                        title: 'Success',
                        message: 'Berhasil menambahkan tamu',
                        position: 'bottomLeft',
                        color:'green'
                    });
                } else {
                    $("#loadingButtonUpdate").addClass('d-none');
                    $("#textButtonUpdate").removeClass('d-none');
                    Swal.fire(
                        'Error!',
                        'Error Input Tamu!',
                        'error'
                    );
                }
            },
        })
    })

    $(document).on("click",".delete",function(e){
    Swal.fire({
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-light'
            },
            title: 'Apakah anda yakin?',
            text: "Apakah anda yakin ingin menghapus data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete'
        }).then((result) => {
            if (result.isConfirmed) {
                e.preventDefault();
                var id = $(this).data("id");
                var token = $("meta[name='csrf-token']").attr("content");
                var url = e.target;
                var url = "{{ route('guest.delete', ":id") }}" ;
                    url = url.replace(':id', id);

                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data: {
                        _token: token,
                        id: id
                    },
                    success: function (response) {
                        $("#success").html(response.message)
                        Swal.fire({
                            customClass: {
                                confirmButton: 'btn btn-danger',
                            },
                            title: 'Success',
                            text: "Tamu telah dihapus",
                            icon: 'success',
                            confirmButtonText: 'OK'
                        })
                        $('#tableGuest').DataTable().ajax.reload();
                    }
                });
                return false;
            }
        })
    });
</script>
@endsection
