<!DOCTYPE html>

<html lang="en">

 <head>

   @include('includes.home.head')

 </head>

 <body>
      {{-- @include('includes.home.navbar') --}}
      {{-- @include('includes.home.header') --}}

      @yield('content')

      {{-- @include('includes.home.footer') --}}

      @include('includes.home.script')
 </body>

</html>
