<?php

use App\Http\Controllers\GuestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [GuestController::class, 'index'])->name('dashboard');
Route::get('/data/{id?}', [GuestController::class, 'showData'])->name('show.data');

Route::post('/guest/post', [GuestController::class, 'store'])->name('guest.post');
Route::post('/guest/full/post', [GuestController::class, 'fullStore'])->name('guest.full.post');

Route::get('/guest/get/{id}', [GuestController::class, 'getDataGuest'])->name('guest.get.data');
Route::post('/guest/update/{id}', [GuestController::class, 'update'])->name('guest.update');

Route::delete('/guest/delete/{id}', [GuestController::class, 'destroy'])->name('guest.delete');

