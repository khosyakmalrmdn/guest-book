<?php

namespace App\Http\Controllers;

use App\Models\Guests;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class GuestController extends Controller
{
    public function index()
    {
        $data = Guests::get();
        if (request()->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                // ->addColumn('date', function($item){
                //     return $item->toDateTimeString();
                // })
                ->make(true);
        }

        return view('home.index');
    }

    public function showData($id = null)
    {
        if($id == 'izinmasuk') {
            $data = Guests::get();
            if (request()->ajax()) {
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('date', function($item){
                        return $item->created_at->toDateTimeString();
                    })
                    ->editColumn('nominal', function($item){
                        return number_format($item->nominal, 0, ",", ".");
                    })
                    ->addColumn('action', function($item){
                        $render =
                        '
                            <div class="d-flex justify-content-start">
                            <button type="button" class="add     m-1 btn btn-warning">Tambah</button>
                            <button type="button" data-id="'.$item->id.'" class="edit m-1 btn btn-success">Edit</button>
                            <button type="button" data-id="'.$item->id.'" class="delete m-1 btn btn-danger">Hapus</button>
                            </div>
                        ';
                        return $render;
                    })
                    ->rawColumns(['action', 'nominal'])
                    ->make(true);
            }

            return view('home.data');
        } else {
            return abort(404);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'address'   => 'required'
            ]);

            if($validator->fails()) {

                return response()->json([
                    'status' => '500',
                    'error' => $validator->getMessageBag()->toArray()
                ], 500);

            } else {

                $guest = Guests::create([
                    'name'      => $request->name,
                    'address'   => $request->address
                ]);

                return response()->json([
                    'status' => '200',
                    'message' => 'Success add guest',
                ], 200);
            }

        } catch (Exception $err) {
            return response()->json([
                'status' => '500',
                'error' => 'Error Input'
            ], 500);
        }
    }

    public function fullStore(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'address'   => 'required',
                'nominal'   => 'required'
            ]);

            if($validator->fails()) {

                return response()->json([
                    'status' => '500',
                    'error' => $validator->getMessageBag()->toArray()
                ], 500);

            } else {

                $guest = Guests::create([
                    'name'      => $request->name,
                    'address'   => $request->address,
                    'nominal'   => \Str::remove('.', $request->nominal)
                ]);

                return response()->json([
                    'status' => '200',
                    'message' => 'Success add guest',
                ], 200);
            }

        } catch (Exception $err) {
            return response()->json([
                'status' => '500',
                'error' => 'Error Input'
            ], 500);
        }
    }

    public function getDataGuest($id){
        $guest = Guests::findOrFail($id);
        return json_encode($guest);
    }

    public function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'address'   => 'required',
                'nominal'   => 'required'
            ]);

            if($validator->fails()) {

                return response()->json([
                    'status' => '500',
                    'error' => $validator->getMessageBag()->toArray()
                ], 500);

            } else {

                $guest = Guests::find($id)->update([
                    'name'      => $request->name,
                    'address'   => $request->address,
                    'nominal'   => \Str::remove('.', $request->nominal)
                ]);

            }


            return response()->json([
                'status' => '200',
                'message' => 'Success update guest',
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => '500',
                'error' => $err->getMessage()
            ], 500);
        }
    }

    public function destroy($id)
    {
        try{
            $guest = Guests::find($id);
            $guest->delete();
            return response()->json([
                'status' => '200',
                'message' => 'Success delete guest',
            ], 200);
        } catch (Exception $err) {
            return response()->json([
                'status' => '500',
                'error' => $err->getMessage()
            ], 500);
        }
    }
}
